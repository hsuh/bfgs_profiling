#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"

    export "$KEY"="$VALUE"
done

#Inputs: method={cdbfgs,bfgs}, type={inplace,reorder}, dim=int, hist=int, sync={async,sync}a

if [ $method == "cdbfgs" ]; then
  mm="lmvmcdbfgs"
  if [ $type == "inplace" ]; then
    tt="cd_inplace"
    ttt="inplace"
  elif [ $type == "reorder" ]; then
    tt="cd_reorder"
    ttt="reorder"
  fi
elif [ $method == "bfgs" ]; then
  mm="lmvmbfgs"
  type="notype"
  ttt="bfgs"
else
  echo "Method is neither cdbfgs nor bfgs!"
  exit 1      
fi


if [ $sync == "sync" ]; then
  ss=0
  sss="global_blocking"
elif [ $sync == "async" ]; then
  ss=1
  sss="default_blocking"
else
  echo "Neither sync nor async!"
  exit 1      
fi

title=$ttt"_data.txt"

output_string="rosenbrock1"_"$method"_"$type"_dim_"$dim"_hist_"$hist"_"$sync"""
echo data_$output_string

rm temp.txt

if [ $method == "cdbfgs" ]; then
  make test search='tao_unconstrained_tutorials-rosenbrock1cu_cdbfgs_'$type'_timings' EXTRA_OPTIONS="-tao_converged_reason -mat_type aijcusparse -n "$dim" -mat_lbfgs_type "$tt" -tao_lmvm_mat_lmvm_hist_size "$hist" -tao_lmvm_mat_lmvm_internal_device_context "$sss" -log_view_gpu_time -log_view :/home/hsuh/petsc/temp.txt"
#  ./rosenbrock1 -n $dim -tao_lmvm_mat_type $mm -mat_lbfgs_type $tt -cuda -tao_lmvm_mat_lmvm_hist_size $hist -tao_lmvm_mat_lmvm_async $ss -log_view_gpu_time -log_view >>  temp.txt #$output_string.txt
  time1=$(awk '/^CDBFGSMatSolve/ {print $4}' temp.txt)
  time2=$(awk '/^MatLMVMUpdate/ {print $4}' temp.txt)
  iter=$(awk '/^MatLMVMUpdate/ {print $2}' temp.txt)
else 
  make test search='tao_unconstrained_tutorials-rosenbrock1cu_bfgs_timings' EXTRA_OPTIONS="-tao_converged_reason -mat_type aijcusparse -n "$dim" -tao_lmvm_mat_lmvm_hist_size "$hist" -tao_lmvm_mat_lmvm_internal_device_context "$sss" -log_view_gpu_time -log_view :/home/hsuh/petsc/temp.txt"
#  ./rosenbrock1 -n $dim -tao_lmvm_mat_type $mm -cuda -tao_lmvm_mat_lmvm_hist_size $hist -tao_lmvm_mat_lmvm_async $ss -log_view_gpu_time -log_view >>  temp.txt #$output_string.txt
  time1=$(awk '/^BFGSMatSolve/ {print $4}' temp.txt)
  time2=$(awk '/^MatLMVMUpdate/ {print $4}' temp.txt)
  iter=$(awk '/^MatLMVMUpdate/ {print $2}' temp.txt)
fi

echo $time1
echo $time2
echo $iter

totaltime=$(awk 'BEGIN { print '$time1'+'$time2'}')

dataa=$dim"	"$hist"	"$sync"	"$totaltime"	"$iter
echo $dataa

#echo aste -d " " dataa master_data.txt



echo $dataa >> $title
