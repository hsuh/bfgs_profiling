#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"

    export "$KEY"="$VALUE"
done

#Inputs: location={home, device}, method={cdbfgs,bfgs}, type={inplace,reorder}, dim=int, hist=int, epochs=int, iters=int directory=string

log_file="${directory}/rosenbrock_${location}_${method}_${type}_dim_${dim}_hist_${hist}_np_${NP}.log"

extra_args="-tao_lmvm_mat_lmvm_hist_size $hist -root_device_context_stream_type default_blocking -log_view :${log_file} -n ${dim} -tao_smonitor 0"

if [ $location == "device" ]; then
  extra_args="$extra_args -mat_type aijhipsparse"
else
  extra_args="$extra_args -mat_type aij"
fi

if [ $method == "cdbfgs" ]; then
  extra_args="$extra_args -tao_lmvm_mat_type lmvmcdbfgs"
  if [ $type == "inplace" ]; then
    extra_args="$extra_args -tao_lmvm_mat_lbfgs_type cd_inplace"
  elif [ $type == "reorder" ]; then
    extra_args="$extra_args -tao_lmvm_mat_lbfgs_type cd_reorder"
  fi
else
  extra_args="$extra_args -tao_lmvm_mat_type lmvmbfgs -tao_lmvm_mat_lmvm_scale_type none"
fi

if [ $location == "device" ]; then
  PETSC_ARCH=${PETSC_ARCH} make -C ${PETSC_DIR} test search='tao_unconstrained_tutorials-rosenbrock5_2' EXTRA_OPTIONS="$extra_args" TIMEOUT=100000000 NP="${NP}"
else 
  PETSC_ARCH=${PETSC_ARCH} make -C ${PETSC_DIR} test search='tao_unconstrained_tutorials-rosenbrock4_2' EXTRA_OPTIONS="$extra_args" TIMEOUT=100000000 NP="${NP}"
fi
echo "bfgs_profiling PETSc commit: $(cd $PETSC_DIR && git describe --always --dirty)" >> $log_file

