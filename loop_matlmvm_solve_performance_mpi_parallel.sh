#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"

    export "$KEY"="$VALUE"
done

#bash directory=path PETSC_DIR=path

location=("host" "device")
hist=(5 10 20 50)
#dim=(1000 10000 100000 1000000 10000000 100000000)
dim=(1000 10000 100000 1000000 10000000)
method=("bfgs" "cdbfgs")
type=("inplace" "reorder")
epochs=1
loc="host"
mpisize=(8)
iters=100

for h in "${hist[@]}"
do
  for d in "${dim[@]}"
  do
    bash ./collect_matlmvm_solve_performance.sh "NP=${HOST_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=inplace"
    bash ./collect_matlmvm_solve_performance.sh "NP=${HOST_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=bfgs" "type=reorder"
    bash ./collect_matlmvm_solve_performance.sh "NP=${GPU_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=device" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=inplace"
    bash ./collect_matlmvm_solve_performance.sh "NP=${GPU_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=device" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=bfgs" "type=reorder"
  done
done

#Somehow Frontier's bash screws up -eq stuff on bash...

echo "Entering largest size stuff" 

iters=10
dim=(100000000)
for h in "${hist[@]}"
do
  for d in "${dim[@]}"
  do
    bash ./collect_matlmvm_solve_performance.sh "NP=${HOST_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=inplace"
    bash ./collect_matlmvm_solve_performance.sh "NP=${HOST_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=bfgs" "type=reorder"
    bash ./collect_matlmvm_solve_performance.sh "NP=${GPU_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=device" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=inplace"
    bash ./collect_matlmvm_solve_performance.sh "NP=${GPU_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=device" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=bfgs" "type=reorder"
  done
done
