#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"

    export "$KEY"="$VALUE"
done

#Inputs: method={cdbfgs,bfgs}, type={inplace,reorder}, dim=int, hist=int, location={host,device}

log_file="/ccs/home/hsuh/bfgs_profiling/frontier/solve_performance_${location}_${method}_${type}_dim_${dim}_hist_${hist}_epochs_${epochs}_iters_${iters}.log"

extra_args="-mat_lmvm_hist_size $hist -root_device_context_stream_type default_blocking -log_view :${log_file} -n ${dim} -epochs ${epochs} -iters ${iters}"

if [ $location == "device" ]; then
  extra_args="$extra_args -vec_type hip -random_type random123"
else
  extra_args="$extra_args -vec_type standard -random_type random123"        
fi


if [ $method == "cdbfgs" ]; then
  extra_args="$extra_args -mat_type lmvmcdbfgs"        
  if [ $type == "inplace" ]; then
    extra_args="$extra_args -mat_lbfgs_type cd_inplace"          
  elif [ $type == "reorder" ]; then
    extra_args="$extra_args -mat_lbfgs_type cd_reorder"          
  fi
else
  extra_args="$extra_args -mat_type lmvmbfgs -mat_lmvm_scale_type none"        
fi

cd $PETSC_DIR/src/ksp/ksp/utils/lmvm/tests
make clean
make solve_performance
./solve_performance $extra_args 

