# Example plot generation

- `python3 ./bfgs_profile.py plot --directory=./polaris --output-file=./polaris_gpu.png --filters="vec_type=cuda" --plot-kwargs="title=L-BFGS Update + Solve Iteration (Polaris A100)"`
- `python3 ./bfgs_profile.py plot --directory=./polaris --output-file=./polaris_host.png --filters="vec_type=standard" --plot-kwargs="title=L-BFGS Update + Solve Iteration (Polaris CPU)"`
- `python3 ./bfgs_profile.py plot --directory=./polaris --output-file=./polaris_both.png --plot-kwargs="title=L-BFGS Update + Solve Iteration (Polaris)" --filters="matrix type=cdbfgs+inplace"`
