#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"

    export "$KEY"="$VALUE"
done

#bash directory=path PETSC_DIR=path

location=("host" "device")
hist=(5 10 20 50)
dim=(1000 10000 100000 1000000 10000000 100000000)
method=("bfgs" "cdbfgs")
type=("inplace" "reorder")
loc="host"

for h in "${hist[@]}"
do
  for d in "${dim[@]}"
  do
    bash ./collect_matlmvm_rosenbrock.sh "NP=${HOST_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=inplace"
    bash ./collect_matlmvm_rosenbrock.sh "NP=${HOST_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "dim=${d}" "hist=${h}" "method=bfgs" "type=reorder"
    bash ./collect_matlmvm_rosenbrock.sh "NP=${GPU_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=device" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=inplace"
    bash ./collect_matlmvm_rosenbrock.sh "NP=${GPU_NP}" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=device" "dim=${d}" "hist=${h}" "method=bfgs" "type=reorder"
  done
done

