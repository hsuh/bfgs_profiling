#!/usr/bin/env python3

import pandas
import pathlib
import re
from functools import reduce
from itertools import pairwise
from typing import Union
import altair as alt

re_program_line = re.compile('../(?P<program_name>\w+) on a \S+ named (?P<node>\w+) with (?P<mpi_size>[0-9]+)')
re_petsc_version = re.compile('Using Petsc (Release Version (?P<petsc_release_version>\S+),|Development GIT revision: (?P<petsc_commit_version>\S+))')
re_stage = re.compile(' 1: (?P<stage_name>.+): (?P<stage_time>\S+)')
#re_stage = re.compile('MatSolve \S+ \S+ \S+ \S+ (?P<stage_time>\S+)')
#re_stage = re.compile('MatSolve + (?P<stage_time>\S+)')
re_taosolve_time = re.compile('TaoSolve               \S+ \S+ (?P<solve_time>\S+)')
re_iter = re.compile('MatSolve              (?P<num_iter>[0-9]+)')
re_hist = re.compile('-tao_lmvm_mat_lmvm_hist_size (?P<num_history_vecs>[0-9]+)')
re_mat_type = re.compile('-mat_type (?P<mat_type>\w+)')
re_lmvm_mat_type = re.compile('-tao_lmvm_mat_type (?P<lmvm_mat_type>\w+)')
re_lbfgs_type = re.compile('-tao_lmvm_mat_lbfgs_type (?P<lbfgs_type>\w+)')
re_vec_size = re.compile('-n (?P<num_variables>[0-9]+)')
re_config = re.compile('End of PETSc Option Table entries(?P<configuration>[\w\W]*)')

metadata = ['node', 'petsc_version', 'configuration']
integer_fields = ['mpi_size', 'num_history_vecs', 'num_variables', 'num_iter']
float_fields = ['stage_time', 'solve_time']

regexes = [
        re_iter,
        re_taosolve_time,
        re_program_line,
        re_petsc_version,
        re_stage,
        re_hist,
        re_mat_type,
        re_lmvm_mat_type,
        re_lbfgs_type,
        re_vec_size,
        re_config
        ]


class BfgsProfile(object):
    def __init__(self, filename: str) -> None:
        with open(filename, 'r') as file:
            data = file.read()
            self.__dict__.update(reduce(lambda a, b: dict(a, **b), [match.groupdict() for regex in regexes for match in regex.finditer(data)]))
#            self.num_total_iters = int(self.num_iters) * int(self.num_epochs)
            self.petsc_version = self.__dict__.get('petsc_release_version', self.__dict__.get('petsc_commit_version'))
            self.filename = str(filename)
            for p in self.__dict__.keys():
                if p in integer_fields:
                    self.__dict__[p] = int(self.__dict__[p])
                elif p in float_fields:
                    self.__dict__[p] = float(self.__dict__[p])

    def mismatch(self, other) -> Union[str,None]:
        for p in metadata:
            if self.__dict__[p] != other.__dict__[p]:
                return p
        return None

    def dataframe(self) -> pandas.DataFrame:
        entries = {}
        for p in self.__dict__:
            if p == 'stage_time' or p in metadata:
                continue
            if p == 'stage_name':
                entries[self.__dict__[p]] = [self.__dict__['stage_time']]
            else:
                entries[p] = [self.__dict__[p]]
        df = pandas.DataFrame(entries)
        df['history size'] = df['num_history_vecs']
#        df['seconds per iteration'] = df[self.__dict__['stage_name']] / df['num_total_iters']
        df['seconds per iteration'] = df['solve_time']
        df['variable iterations per second'] = df['num_variables'] / df['seconds per iteration']
        df['effective bandwidth (GB/s)'] = df['num_variables'] * 8 * (2 * df['num_history_vecs'] + 2) / df['seconds per iteration'] / 1.e9
        df['DOF/s'] = df['num_variables']/df['seconds per iteration']/df['num_iter']
        def matrix_type(row):
            if row['lmvm_mat_type'] == 'lmvmbfgs':
                return 'bfgs'
            else:
                if row['lbfgs_type'] == 'cd_reorder':
                    return 'cdbfgs+reorder'
                else:
                    return 'cdbfgs+inplace'
        def location(row):
            if row['mat_type'] == 'aijhipsparse':
                return 'device'
            elif row['mat_type'] == 'aijcusparse':
                return 'device'
            else:
                return 'host'
        df['location'] = df.apply(location, axis=1)
        df['matrix type'] = df.apply(matrix_type, axis=1)
        return df


def altair_plot(df: pandas.DataFrame, stage_name, filters: dict, **kwargs) -> alt.Chart:
    df = df.copy()
    for key, value in filters.items():
        df = df.loc[df[key] == value]
    chart = alt.Chart(df).mark_point().encode(
        x=alt.X('seconds per iteration').scale(type='log'),
#        y=alt.Y('effective bandwidth (GB/s):Q').scale(type='log').axis(format=".1e"),
        y=alt.Y('DOF/s').scale(type='log').axis(format=".1e"),
        color=alt.Color('matrix type').scale(scheme='dark2'),
        size=alt.Size('num_history_vecs:O', title='history size', scale=alt.Scale(type='point', range=[10,100])),
        shape='location'
        ).properties(**kwargs)
    return chart


def create_dataframe(path: pathlib.Path) -> pandas.DataFrame:
    import os
    bfgs_profiles = [BfgsProfile(path.joinpath(filename)) for filename in os.listdir(path)]
    for a,b in pairwise(bfgs_profiles):
        mismatch_field = a.mismatch(b)
       # if mismatch_field:
       #     raise RuntimeError(f'Profile logs {a.filename} and {b.filename} differ in property {mismatch_field}')
    df = pandas.concat([bp.dataframe() for bp in bfgs_profiles], ignore_index=True)
    try:
        for p in metadata:
            df.attrs[p] = bfgs_profiles[0].__dict__[p]
    except IndexError:
        pass
    return df


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Create a CSV file from a directory of BFGS profiling data stored as PETSc log files")
    parser.add_argument("action")
    parser.add_argument("-d", "--directory", type=pathlib.Path)
    parser.add_argument("-o", "--output-file", type=pathlib.Path)
    #parser.add_argument("-s", "--stage-name", type=str, nargs='?', default='LMVM MatSolve Loop')
    parser.add_argument("-s", "--stage-name", type=str, nargs='?', default='Rosenbrock solve')
    parser.add_argument("-f", "--filters", type=str, nargs='?', default='')
    parser.add_argument("-p", "--plot-kwargs", type=str, nargs='?', default='')

    args = parser.parse_args()

    df = create_dataframe(args.directory)
    if args.action == 'plot':
        stage_name = args.stage_name
        filters = {}
        if args.filters:
            filter_tokens = args.filters.split(',')
            for token in filter_tokens:
                left_right = token.split('=')
                filters[left_right[0]] = left_right[1]
        kwargs = {}
        if args.plot_kwargs:
            kwarg_tokens = args.plot_kwargs.split(',')
            for token in kwarg_tokens:
                left_right = token.split('=')
                kwargs[left_right[0]] = left_right[1]
        plot = altair_plot(df, stage_name, filters, **kwargs)
        plot.save(args.output_file)
    if args.action == 'pgfplots_csv_dir':
        directory = args.output_file
        pathlib.Path(directory).mkdir(parents=True, exist_ok = True)
        for location in set(df['location']):
            df_l = df.loc[df['location'] == location]
            df_l = pandas.DataFrame(df_l, columns=['solve_time','DOF/s','location','matrix type','history size','num_variables','mpi_size'])
            for matrix_type in set(df_l['matrix type']):
                df_m = df_l.loc[df_l['matrix type'] == matrix_type]
                df_m = df_m.sort_values(by=['history size', 'num_variables', 'mpi_size'])
                for mpisize in set(df_m['mpi_size']):
                    df_mpi = df_m.loc[df_l['mpi_size'] == mpisize]
                    df_mpi = df_mpi.sort_values(by=['history size', 'num_variables', 'mpi_size'])
                    for n in set(df_mpi['num_variables']):
                        df_v = df_mpi.loc[df_mpi['num_variables'] == n]
                        filename = pathlib.Path(directory).joinpath("{0}.{1}.{2}.np{3}.n{4}.csv".format(pathlib.PurePath(args.directory).name, location, matrix_type, mpisize, n))
                        df_v.to_csv(filename, index=False)
                    for h in set(df_mpi['history size']):
                        df_v = df_mpi.loc[df_mpi['history size'] == h]
                        filename = pathlib.Path(directory).joinpath("{0}.{1}.{2}.np{3}.h{4}.csv".format(pathlib.PurePath(args.directory).name, location, matrix_type, mpisize, h))
                        df_v.to_csv(filename, index=False)

    if args.action == 'csv':
        df.to_csv(args.output_file, index=False)

